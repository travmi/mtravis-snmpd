#snmpd

####Table of Contents

1. [Overview](#overview)
2. [Setup - The basics of getting started with snmpd](#setup)
    * [What snmpd affects](#what-snmpd-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with snmpd](#beginning-with-snmpd)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)
7. [License - License information](#license)

##Overview

The snmp module installs, configures, and manages the snmpd service.

##Setup

###What snmpd affects
* snmpd package.
* snmpd configuration file.
* snmpd service.

###Beginning with snmpd

```puppet
include snmpd
```

##Usage

All interaction with the snmpd module can do be done through the main snmpd class.

###I just want snmp, what's the minimum I need?

```puppet
include snmpd
```

##Reference

###Classes

####Public Classes

* snmpd: Main class, includes all other classes.

####Private Classes

* snmpd::package: Handles the snmpd package install.
* snmpd::config: Handles the configuration files.
* snmpd::service: Handles the service.

###Parameters for snmpd

The following parameters are available in the snmpd module:

####There are currently no paramters for this class.

##Limitations

This module has been built on and tested against Puppet Enterprise 3.3 and higher.

The module has been tested on:

* CentOS 6.5

Support on other platforms has not been tested and is currently not supported. 

##Development
Copyright (C) 2014 Mike Travis

For the relevant commits Copyright (C) by the respective authors.

Can be contacted at: mike.r.travis@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

