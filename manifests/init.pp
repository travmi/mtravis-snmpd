# == Class: snmpd
#
# This class manages the installation, configuration and snmpd service.
#
# === Parameters
#
# Document parameters here.
#
# There are currently no parameters for this module.
#
# === Examples
#
#  include snmpd
class snmpd (
   
  
){

 anchor { 'snmpd::begin': } ->
    class  { '::snmpd::package': } ->
    class  { '::snmpd::config': } ~>
    class  { '::snmpd::service': } ->
 anchor { 'snmpd::end': }

}
