# Class: snmpd::service
#
# This module manages the snmpd service.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly.
class snmpd::service {
  
  service { 'snmpd':
    ensure => 'running',
    enable => true,
  }

}