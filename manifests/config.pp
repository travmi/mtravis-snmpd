# Class: snmpd::config
#
# This module manages the snmpd config.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly.
class snmpd::config {
  
  file { '/etc/snmp/snmpd.conf':
    ensure  => 'file',
    content => template('snmpd/snmpd.conf.erb'),
    mode    => '0644',
  }
   
  file { '/usr/local/bin/apache_status.pl':
    ensure => 'file',
    source => 'puppet:///modules/snmpd/apache_status.pl',
    mode   => '0755',
   }
   
  file { '/usr/local/bin/snmpdiskio':
    ensure => 'file',
    source => 'puppet:///modules/snmpd/snmpdiskio',
    mode   => '0755',
   }
   
  file { '/etc/logrotate.d/snmpd':
    ensure => 'file',
    source => "puppet:///modules/snmpd/snmpd",
    mode   => '0644'    
   }

# This directory is needed in order for the apache_status.pl
# script to write a tmp file to the directory.   
  file { '/var/cache/snmp':
    ensure => 'directory',
    mode   => '0644',
   }
   
}