# Class: snmpd::package
#
# This module manages the snmpd package.
#
# Parameters:
#
# There are no default parameters for this class.
#
class snmpd::package {
  
  package { 'net-snmp':
     ensure => 'latest',
  }
  
  package { 'net-snmp-utils':
    ensure => 'latest',
  }
  
}